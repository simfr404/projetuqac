import sys

sys.path.append("/usr/local/lib/python2.7/site-packages")
import graph_tool.community as cm
import graph_tool.draw as draw
import graph_tool as gt
import networkx as nx
from networkx.readwrite import json_graph
import json

graphdataset = "ExtraitBilletterie-SansSalles.graphml"
useGraphTool = False

graph = nx.read_graphml(graphdataset)
ids = {}
for node in graph.nodes():
	ids[node] = str(node)
nx.set_node_attributes(graph, "name", ids)
nx.write_graphml(graph, graphdataset)

print(nx.info(graph))

data = json_graph.node_link_data(graph)
with open(graphdataset.replace(".graphml", ".json"), 'w') as out:
	json.dump(data, out, indent=4)

if useGraphTool:
	h = gt.load_graph(graphdataset)
	state = cm.minimize_nested_blockmodel_dl(h)
	draw.draw_hierarchy(state, output=graphdataset.replace(".graphml", "_nestedblocks.pdf"))

	state2 = cm.minimize_blockmodel_dl(h)
	labels = h.properties[("v", "name")]
	draw.graph_draw(h, vertex_text=labels, vertex_fill_color=state2.b, vertex_shape=state2.b, output=graphdataset.replace(".graphml", "_blocks.pdf"))
