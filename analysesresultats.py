from __future__ import division

import json

import networkx as nx
from networkx.readwrite import json_graph

filediff = "comparemethods.txt"
jsonfiles = ["scalefree_influencersAdjacencyMatrix.json", "scalefree_influencersAdjacencyMatrixWithBlocks.json", "scalefree_neighbouringInfluencersWithoutBinaryVariables.json"]

d_adj = json.load(open(jsonfiles[0]))
g_adj = json_graph.node_link_graph(d_adj)

d_neigh = json.load(open(jsonfiles[2]))
g_neigh = json_graph.node_link_graph(d_neigh)

influences_adj = nx.get_node_attributes(g_adj, "influence")
influences_neigh = nx.get_node_attributes(g_neigh, "influence")

inf_adj = [x for x in influences_adj if influences_adj[x] > 0]
inf_neigh = [x for x in influences_neigh if influences_neigh[x] > 0]
inf_order_adj = len(inf_adj)
inf_order_neigh = len(inf_neigh)

diff_adj = list(set(inf_adj) - set(inf_neigh))
diff_neigh = list(set(inf_neigh) - set(inf_adj))
print(diff_adj)
print(diff_neigh)

print(len(diff_adj))
print(len(diff_neigh))

print(inf_order_adj)
print(inf_order_neigh)

avgindegree_adj = 0
avgoutdegree_adj = 0
for u in influences_adj:
	if influences_adj[u] > 0:
		avgindegree_adj += len(g_adj.predecessors(u))
		avgoutdegree_adj += len(g_adj.successors(u))
avgindegree_adj = avgindegree_adj / inf_order_adj
avgoutdegree_adj = avgoutdegree_adj / inf_order_adj

print(avgindegree_adj)
print(avgoutdegree_adj)

avgindegree_neigh = 0
avgoutdegree_neigh = 0
for u in influences_neigh:
	if influences_neigh[u] > 0:
		avgindegree_neigh += len(g_neigh.predecessors(u))
		avgoutdegree_neigh += len(g_neigh.successors(u))
avgindegree_neigh = avgindegree_neigh / inf_order_neigh
avgoutdegree_neigh = avgoutdegree_neigh / inf_order_neigh

print(avgindegree_neigh)
print(avgoutdegree_neigh)

hits_adj, hubs_adj = nx.hits(g_adj)
hits_neigh, hubs_neigh = nx.hits(g_neigh)

avg_hits_adj = 0
avg_hubs_adj = 0
for u in influences_adj:
	if influences_adj[u] > 0:
		avg_hits_adj += hits_adj[u]
		avg_hubs_adj += hubs_adj[u]

avg_hits_neigh = 0
avg_hubs_neigh = 0
for u in influences_neigh:
	if influences_neigh[u] > 0:
		avg_hits_neigh += hits_neigh[u]
		avg_hubs_neigh += hubs_neigh[u]

avg_hits_adj = avg_hits_adj / inf_order_adj
avg_hubs_adj = avg_hubs_adj / inf_order_adj

avg_hits_neigh = avg_hits_neigh / inf_order_neigh
avg_hubs_neigh = avg_hubs_neigh / inf_order_neigh

print(avg_hits_adj)
print(avg_hubs_adj)
print(avg_hits_neigh)
print(avg_hubs_neigh)

with open(filediff, "w") as f:
	inf_adj = [int(x) for x in inf_adj]
	inf_adj.sort()
	inf_neigh = [int(x) for x in inf_neigh]
	inf_neigh.sort()
	diff_adj = [int(x) for x in diff_adj]
	diff_adj.sort()
	diff_neigh = [int(x) for x in diff_neigh]
	diff_neigh.sort()

	f.write("Graphe matrice adjacence \n")
	f.write("Influenceurs : \n")
	for i in inf_adj:
		f.write(str(i) + "\t")
	f.write("\n")
	f.write("Difference avec autre methode \n")
	for d in diff_adj:
		f.write(str(d) + "\t")
	f.write("\n")
	f.write("Degre moyen entrants des influenceurs " + str(avgindegree_adj) + "\n")
	f.write("Degre moyen sortants des influenceurs " + str(avgoutdegree_adj))

	f.write("\n")
	f.write("\n")

	f.write("Graph modele de base voisinage \n")
	f.write("Influenceurs : \n")
	for i in inf_neigh:
		f.write(str(i) + "\t")
	f.write("\n")
	f.write("Difference avec autre methode \n")
	for d in diff_neigh:
		f.write(str(d) + "\t")
	f.write("\n")
	f.write("Degre moyen entrants des influenceurs " + str(avgindegree_neigh) + "\n")
	f.write("Degre moyen sortants des influenceurs " + str(avgoutdegree_neigh))
